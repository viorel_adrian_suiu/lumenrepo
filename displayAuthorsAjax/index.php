<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<div class="display">
    <div class="username" style="float: left;"></div>
    <div class="email" style="float: left;"></div>
    <div class="location" style="float: left"></div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        var api_url = 'http://192.168.0.101:8000/api/authors/';
        $.ajax({
            type: 'GET',
            url: api_url,
            dataType: 'json',
            crossOrigin: true,
            success: function (data) {
                $.each(data, function(index, value) {
                    $(".username").append('<div>' + value.name + '</div>');
                    $(".email").append('<div>' + value.email + '</div>');
                    $(".location").append('<div>' + value.location + '</div>');
                })
            },
            error: function (jqXHR, text, errorThrown) {
                console.log(jqXHR + " " + text + " " + errorThrown);
            }
        });
    });
</script>