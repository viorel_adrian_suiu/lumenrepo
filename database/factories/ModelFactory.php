<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//$factory->define(App\User::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->email,
//    ];
//});

$factory->define(App\Author::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'github' => $faker->text(20),
        'twitter' => $faker->text(20),
        'location' => $faker->text(20),
        'latest_article_published' => $faker->text(100),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime()
    ];
});
