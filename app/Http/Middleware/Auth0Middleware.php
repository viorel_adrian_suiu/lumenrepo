<?php
/**
 * Created by PhpStorm.
 * User: Vio
 * Date: 4/30/2019
 * Time: 3:42 PM
 */

namespace App\Http\Middleware;

use Closure;
use Auth0\SDK\JWTVerifier;

class Auth0Middleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->hasHeader('Authorization')) {
            return response()->json('Authorization Header not found', 401);
        }

        $token = $request->bearerToken();

        if($request->header('Authorization') == null || $token == null) {
            return response()->json('No token provided', 401);
        }

        $this->retrieveAndValidateToken($token);

        return $next($request);
    }

    public function retrieveAndValidateToken($token)
    {
        try {
            $verifier = new JWTVerifier([
                'supported_algs' => ['RS256'],
                'client_secret' => ['75ww843RzAtLGSaIy4jM8R72r0MhV2jSTXSPSm8W8xRdFgrOg2w3s-ZKeh86OvPu'],
                'valid_audiences' => ['http://192.168.0.101:8000/api/authors/'],
                'authorized_iss' => ['https://dev-9ao-83ov.eu.auth0.com/']
            ]);

            $decoded = $verifier->verifyAndDecode($token);
        }
        catch(\Auth0\SDK\Exception\CoreException $e) {
            throw $e;
        };
    }
}